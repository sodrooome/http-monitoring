class NoContentException(Exception):
    code = 404
    message = "It seems the response is empty"


class BadGatewayException(Exception):
    code = 502
    message = "Failed to sending the email due of run into some trouble"
