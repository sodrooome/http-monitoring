# this is just for example only, remove with your considerable value
HTTP_MONITORING_CONFIG = {
    "DEFAULT_HTTP_TIMEOUT": 5,
    "DEFAULT_HTTP_STATUS_CODE": 200,
    "DEFAULT_HTTP_CONFIG": "",
    "DEFAULT_HTTP_RETRIES": 3,
    "DEFAULT_BACKOFF_FACTOR": 0.2,
    "DEFAULT_ERROR_CODES": 504,
    "MAX_HTTP_TARGET": 5,
}

# this is just for example only, remove with your considerable value
MAILTRAP_CONFIG = {
    "MAIL_SERVER": "sandbox.smtp.mailtrap.io",
    "MAIL_PORT": 2525,
    "MAIL_USERNAME": "",
    "MAIL_PASSWORD": "",
    "MAIL_USE_TLS": True,
    "MAIL_USE_SSL": False,
}

# this is just for example only, remove with your considerable value
EMAIL_RECIPIENTS_CONFIG = {
    "EMAIL_SUBJECT": "Your target endpoint was down! please check your site",
    "EMAIL_SENDER": "ryanfeb@mailtrap.io",
    "EMAIL_RECIPIENT": "ryanfeb@mailtrap.io",
}
