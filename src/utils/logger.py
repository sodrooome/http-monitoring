import logging
from logging import Logger


def http_logging() -> Logger:
    logger = logging.getLogger("http_monitoring_logging")
    handler = logging.StreamHandler()
    handler.setFormatter(
        logging.Formatter(
            fmt="%(asctime)s : %(filename)s : %(funcName)s : %(message)s",
            datefmt="%d-%m-%Y %I:%M:%S",
        )
    )
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    return logger
