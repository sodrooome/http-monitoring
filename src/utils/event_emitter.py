import time

from .logger import http_logging


class HttpEventEmitter:
    # basic event emitter, eventually all the information
    # that retrieved when making HTTP request should be
    # collected and passed into callback arguments, but
    # at the moment i just want to put it as log information

    def __init__(self) -> None:
        self.listeners = {}
        self.log = http_logging()
        self.error_raises = 0

    def on(self, event: str, listener: str) -> None:
        if event not in self.listeners:
            self.listeners[event] = []
        self.log.info(
            f"[INFO] add '{event}' as new event name to the list of listeners"
        )
        self.listeners[event].append(listener)

    def emit(self, event: str, *args, **kwargs) -> None:
        if event not in self.listeners:
            self.error_raises += 1
            self.log.warning(
                f"[WARNING] The {event} name weren't in the list of listeners",
                self.error_raises,
            )

        if event in self.listeners:
            for listener in self.listeners[event]:
                self.log.info(f"[INFO] Call '{event}' event from list of listeners")
                listener(*args, **kwargs)

    def handle_request(self) -> None:
        self.log.info(
            "[INFO] The recent event name will be triggered at once, please wait a while"
        )

    def handle_on_request(self, event: str) -> None:
        self.log.info(
            "[INFO] The event emitter will be triggered to the certain target"
        )
        self.on(event, self.handle_request)  # noqa

    def handle_emit_request(self, event: str) -> None:
        self.log.info(f"[INFO] Currently, we are emitted {event} name")

        # measuring HTTP request until its complete,
        # i think it would be better to use ctime or
        # elapsed() method instead using this (to get more accurate result)
        start_time = time.time()
        end_time = time.time()
        response_time = start_time - end_time
        self.emit(event)
        self.log.info(
            f"[INFO] Request was completed to the certain target within {response_time} ms"
        )
