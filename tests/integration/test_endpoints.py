import unittest
from src.app import app


class TestEndpoint(unittest.TestCase):
    def setUp(self) -> None:
        self.app = app.test_client()

    @unittest.expectedFailure
    def test_create_new_endpoint(self):
        payload = {
            "targets": [
                {
                    "title": "My Personal Blog - Test from Postman",
                    "endpoint": "https://www.google.com/",
                    "expected_http_code": 200,
                }
            ]
        }

        response = self.app.post("/status", payload)
        self.assertEqual(201, response.status_code)
        self.assertEqual(dict, type(response.json))

    def test_fetch_endpoint(self):
        response = self.app.get("/status")
        self.assertEqual(200, response.status_code)
        self.assertEqual(dict, type(response.json))

    def test_fetch_queue_status(self):
        response = self.app.get("/status/queue")
        self.assertEqual(200, response.status_code)

        expected_response = {
            "category": "success",
            "data": {
                "end_time": response.json()["end_time"],
                "queue_status": False,
                "start_time": response.json()["start_time"],
            },
            "message": "Success get the response",
        }
        self.assertEqual(expected_response, response.json)

    def test_create_new_queue(self):
        payload = {"worker_thread": 2, "queue_size": 4}

        response = self.app.post("/status/queue", payload)
        self.assertTrue(201, response.status_code)

    def test_send_email_notifications(self):
        response = self.app.get("/emails")
        self.assertTrue(200, response.status_code)


if __name__ == "__main__":
    unittest.main()
